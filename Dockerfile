FROM fedora

ARG KDE_RUNTIME_VERSION=5.15-21.08

RUN \
    dnf install -y --nogpgcheck \
        flatpak \
        flatpak-builder \
        libappstream-glib \
        which \
        curl \
    && \
    \
    flatpak remote-add \
    --if-not-exists \
        flathub https://flathub.org/repo/flathub.flatpakrepo \
    && \
    \
    flatpak install -y flathub org.kde.Platform//${KDE_RUNTIME_VERSION} \
    && \
    \
    flatpak install -y flathub org.kde.Sdk//${KDE_RUNTIME_VERSION}
